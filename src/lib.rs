#![no_std]

use core::cell::UnsafeCell;
pub use thread_borrow::*;

pub struct RwLock<T> {
    inner: UnsafeCell<T>,
}

impl<T> RwLock<T> {
    pub const fn new(value: T) -> Self {
        Self { inner: UnsafeCell::new(value) }
    }
}

impl<T> RwLock<T> {
    pub fn write<'st>(&self, st: SingleThread<'st>) -> &'st mut T {
        _ = st;

        // SAFETY: singlethreaded access
        unsafe { &mut *self.inner.get() }
    }

    pub const fn read<'mt>(&self, mt: MultiThread<'mt>) -> &'mt T {
        _ = mt;

        // SAFETY: multithreaded access
        unsafe { & *self.inner.get().cast_const() }
    }
}

// SAFETY: mutable access only happens in single thread, shared - in multithread.
unsafe impl<T> Sync for RwLock<T> {}

/// Convenience macro that uses .reborrow() on st
#[macro_export]
macro_rules! write {
    ($lock:expr, $st:expr) => {{
        let lock: &$crate::RwLock<_> = $lock;
        let st: &mut $crate::SingleThread = &mut $st;
        lock.write(st.reborrow())
    }};
}

/// This macro accompanies the write! macro
#[macro_export]
macro_rules! read {
    ($lock:expr, $mt:expr) => {{
        let lock: &$crate::RwLock<_> = $lock;
        lock.read($mt)
    }};
}
